#include "sensorreader.h"
#include <QDebug>

SensorReader::SensorReader(QObject *parent)
    : QObject{parent}
{
    rotation.setSkipDuplicates(true);
    connect(&rotation, &QRotationSensor::readingChanged, this, &SensorReader::readRotation);
    qDebug() << "\n------- Sensor reader connected\n";
    if(!rotation.start())
        qDebug() << "\n------- Unable to start rotation sensor";
    else
        qDebug() << "\n------- Rotation sensor started";
}

void SensorReader::readRotation() {
    emit rotationChanged(rotation.reading()->z());
}
