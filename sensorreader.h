#ifndef SENSORREADER_H
#define SENSORREADER_H

#include <QObject>
#include <QSensor>
#include <QRotationSensor>>

class SensorReader : public QObject
{
    Q_OBJECT
public:
    explicit SensorReader(QObject *parent = nullptr);
    void readRotation();

private:
    QRotationSensor rotation = QRotationSensor(this);

signals:
    void rotationChanged(qreal degrees);

};

#endif // SENSORREADER_H
