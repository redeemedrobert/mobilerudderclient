import QtQuick 2.15
import QtQuick.Window 2.15

Window {
    width: 640
    height: 480
    visible: true
    title: qsTr("MobileRudder")
    Text {
        anchors.centerIn: parent
        id: rotationReadout
        text: qsTr("Rotation: 0")
        Connections {
            target: DeviceRotation
            function onRotationChanged(degrees) {
                //  console.log("Compass changed:", degrees);
                rotationReadout.text = "Rotation: " + degrees
            }
        }
    }
}
